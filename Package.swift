// swift-tools-version:5.1

import PackageDescription

let package = Package(
	name: "Hermes",
	platforms: [
		.iOS(.v12)
	],
	products: [
		.library(
			name: "Hermes",
			targets: ["Hermes"]),
	],
	dependencies: [
		.package(url: "https://github.com/microsoft/appcenter-sdk-apple.git", .upToNextMinor(from: "3.2.0"))
	],
	targets: [
		.target(
			name: "Hermes",
			dependencies: ["AppCenterAnalytics", "AppCenterCrashes"]
		),
		.testTarget(
			name: "HermesTests",
			dependencies: ["Hermes"]),
	]
)
