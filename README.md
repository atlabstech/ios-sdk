# 📡 Hermes

Hermes is a Swift library to standarize the events and logs from the app.

[![Build Status](https://dev.azure.com/atlabsdigital/Atlabs/_apis/build/status/atlabstech.ios-sdk?branchName=master)](https://dev.azure.com/atlabsdigital/Atlabs/_build/latest?definitionId=4&branchName=master)
[![Swift Package Manager](https://img.shields.io/badge/swift%20package%20manager-compatible-brightgreen.svg)](https://swift.org/package-manager/)
[![License](https://img.shields.io/badge/License-MIT-yellow.svg)](https://github.com/tuist/acho/blob/master/LICENSE.md)

## Install 🛠

### Swift Package Manager

Add the dependency in your `Package.swift` file:

```swift
let package = Package(
    name: "myBillionareProject",
    dependencies: [
        .package(url: "https://user:bitbucket_secret@bitbucket.org/atlabstech/ios-sdk.git")),
	],
    targets: [
        .target(
            name: "myBillionareProject",
            dependencies: ["Hermes"]),
        ]
)
```

## Usage 🚀
Create an instance of  `Hermes` passing the services you want to log. There are some services created by default like `App Center`. The options need to conform the `EventsService`.

```swift
let hermes = Hermes(eventsServices: [AppCenterService(key: "mY_t0P_s3Cr3t_k3Y")])
```

By default `Hermes` logs automatically:

- When view controller is load `viewDidLoad`
- When view controller did appear `viewDidAppear(:)`
- When view controller deinited `deinit`
- When a button has an event
- When a switch is toggled

