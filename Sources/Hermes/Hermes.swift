import UIKit

public final class Hermes {
	
	// MARK: - Properties
	/// Collection of services where events and will be sent
	public let eventsServices: [EventsService]

	/// User linked with events. Used to send through different services to create the proper filters
	public var user: String? { didSet { eventsServices.forEach { $0.user = user }}}
	
	// MARK: - Inits
	/// Create a new Hermes instance
	///
	/// - Parameter eventsService: Collection of services where events will be sent
	public init(eventsServices: [EventsService]) {
		self.eventsServices = eventsServices
		UIViewController.classInit
		UIButton.classInit
		logTool = self
	}

	/// Create a new Hermes instance
	///
	/// - Parameter eventsService: Collection of services where events will be sent
	public func send(_ event: LogEvent) {
		eventsServices.forEach { $0.send(event) }
	}
}
