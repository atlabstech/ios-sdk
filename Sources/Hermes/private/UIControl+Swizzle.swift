import UIKit

internal extension UIControl {
	static let classInit: Void = {
		let cls: AnyClass = UIControl.self
		let originalSelector = #selector(sendAction(_:to:for:))
		let newSelector = #selector(swizzled_sendAction(_:to:for:))
		let originalMethod = class_getInstanceMethod(cls, originalSelector)
		let newMethod = class_getInstanceMethod(cls, newSelector)
		method_exchangeImplementations(originalMethod!, newMethod!)
	}()
	
	@objc private func swizzled_sendAction(
		_ action: Selector,
		to target: Any?,
		for event: UIEvent?) {
		
		guard self is ActionLogable else { return }
		
		var properties = [
			"event": event?.type != nil ? "\(event!.type.rawValue)" : "NO_ACTION_EVENT_ASSOCIATED"
		]
		
		if let logable = self as? ActionLogable { properties["identifier"] = logable.identifier }
		
		var title = "\(type(of: self)).action"
		if let vc = viewControllerParent() {
			let typeString = NSStringFromClass(type(of: vc))
			title = "\(typeString).\(title)"
		}
		
		logTool?.send(
			LogEvent(
				title: title,
				properties: properties))
		
		swizzled_sendAction(action, to: target, for: event)
	}
}
