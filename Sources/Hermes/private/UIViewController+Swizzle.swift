import UIKit

internal final class Deallocator {

	var closure: () -> Void

	init(_ closure: @escaping () -> Void) {
		self.closure = closure
	}

	deinit {
		closure()
	}
}

private var associatedObjectAddr = ""

internal extension UIViewController {

	static func testLog() {
		logTool?.send(LogEvent(title: "Test"))
	}

	@objc fileprivate func swizzled_viewDidLoad() {
		let className = NSStringFromClass(type(of: self))
		let deallocator = Deallocator {
			print("\(className) deinit")
			logTool?.send(LogEvent(title: "\(className).deinit"))
		}
		logTool?.send(LogEvent(title: "\(className).viewDidLoad"))
		objc_setAssociatedObject(self, &associatedObjectAddr, deallocator, .OBJC_ASSOCIATION_RETAIN)
	}

	@objc fileprivate func swizzled_viewDidAppear() {
		let className = NSStringFromClass(type(of: self))
		logTool?.send(LogEvent(title: "\(className).viewDidAppear"))
	}

	static let classInit: Void = {
		let originalViewDidLoadSelector = #selector(viewDidLoad)
		let swizzledViewDidLoadSelector = #selector(swizzled_viewDidLoad)
		let originalViewDidAppearSelector = #selector(viewDidAppear(_:))
		let swizzledViewDidAppearSelector = #selector(swizzled_viewDidAppear)

		let forClass: AnyClass = UIViewController.self
		let originalViewDidLoadMethod = class_getInstanceMethod(forClass, originalViewDidLoadSelector)
		let swizzledViewDidLoadMethod = class_getInstanceMethod(forClass, swizzledViewDidLoadSelector)
		let originalViewDidAppearMethod = class_getInstanceMethod(forClass, originalViewDidAppearSelector)
		let swizzledViewDidAppearMethod = class_getInstanceMethod(forClass, swizzledViewDidAppearSelector)
		method_exchangeImplementations(originalViewDidLoadMethod!, swizzledViewDidLoadMethod!)
		method_exchangeImplementations(originalViewDidAppearMethod!, swizzledViewDidAppearMethod!)
	}()
}

