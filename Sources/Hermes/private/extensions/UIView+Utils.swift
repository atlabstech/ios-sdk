import UIKit

internal extension UIView {
	func viewControllerParent() -> UIViewController? {
		if let nextResponder = self.next as? UIViewController {
			return nextResponder
		} else if let nextResponder = self.next as? UIView {
			return nextResponder.viewControllerParent()
		} else {
			return nil
		}
	}
}
