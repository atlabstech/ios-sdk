import UIKit

public protocol ActionLogable {
	var identifier: String { get }
}

extension UISwitch: ActionLogable {
	public var identifier: String { return String(describing: type(of: self)) + "." + "\(isOn ? "on" : "off")" }
}

extension UIButton: ActionLogable  {
	public var identifier: String { return title(for: .normal) ?? titleLabel?.text ?? "NO_TEXT_ASSOCIATED" }
}
