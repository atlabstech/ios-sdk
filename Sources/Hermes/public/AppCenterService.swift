import AppCenter
import AppCenterCrashes
import AppCenterAnalytics

fileprivate extension LogEvent.Flag {

	private static let mapMsFlags: [LogEvent.Flag: MSFlags] = [
		.normal: .normal,
		.critical: .critical,
	]

	var msFlag: MSFlags { LogEvent.Flag.mapMsFlags[self] ?? .normal }
}

fileprivate extension LogLevel {
	private static let mapMsLevels: [LogLevel: MSLogLevel] = [
		.verbose: MSLogLevel.verbose,
		.debug: MSLogLevel.debug,
		.info: MSLogLevel.info,
		.warning: MSLogLevel.warning,
		.error: MSLogLevel.error,
		.assert: MSLogLevel.assert,
		.none: MSLogLevel.none,
	]

	var msLogLevel: MSLogLevel { LogLevel.mapMsLevels[self] ?? .debug }
}

public class AppCenterService: EventsService {

	public let key: String
	public var user: String? { didSet { MSAppCenter.setUserId(user) }}
	public var logLevel: LogLevel = .info { didSet { MSAppCenter.setLogLevel(logLevel.msLogLevel) }}

	public init(
		key: String,
		user: String? = nil) {
		self.user = user
		self.key = key
		MSAppCenter.setLogLevel(logLevel.msLogLevel)
		MSAppCenter.setUserId(user)
		MSAppCenter.start(key, withServices: [MSCrashes.self, MSAnalytics.self])
	}

	public func send(_ event: LogEvent) {
		MSAnalytics.trackEvent(event.title, withProperties: event.properties, flags: event.flag.msFlag)
	}
}
