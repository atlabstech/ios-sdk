public protocol EventsService: class {

	/// User linked with crashes and events
	var user: String? { get set }

	/// Set the log level to the log engine
	var logLevel: LogLevel { get set }

	/// Send a new event to the logs engine
	func send(_ event: LogEvent)
}
