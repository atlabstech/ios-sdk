public struct LogEvent {

	public enum Flag {
		case normal
		case critical
	}

	public var title: String
	public var properties: [String: String]
	public var flag: Flag

	public init(
		title: String,
		properties: [String: String] = [:],
		flag: Flag = .normal) {
		self.title = title
		self.properties = properties
		self.flag = flag
	}
}
