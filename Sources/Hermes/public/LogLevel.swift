public enum LogLevel {
	case verbose
	case debug
	case info
	case warning
	case error
	case assert
	case none
}
