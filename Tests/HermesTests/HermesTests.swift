import XCTest
@testable import Hermes

final class HermesTests: XCTestCase {

	static var allTests = [
		("testInit", testInit),
		("testSendEvent_onlyTitle", testSendEvent_onlyTitle),
		("testSendEvent_onlyTitleProperties", testSendEvent_onlyTitleProperties),
		("testSendEvent_onlyTitlePropertiesFlag", testSendEvent_onlyTitlePropertiesFlag)
	]

	func testInit() {
		let hermes = Hermes(eventsServices: [StubEventsService()])
		XCTAssertEqual(hermes.eventsServices.count, 1)

	}

	func testSendEvent_onlyTitle() {
		let service = StubEventsService()
		let hermes = Hermes(eventsServices: [service])
		hermes.send(LogEvent(title: "Test"))

		XCTAssertEqual(service.events.count, 1)
		XCTAssertEqual(service.events.first!.title, "Test")
		XCTAssertTrue(service.events.first!.properties.isEmpty)
		XCTAssertEqual(service.events.first!.flag, .normal)
	}

	func testSendEvent_onlyTitleProperties() {
		let service = StubEventsService()
		let hermes = Hermes(eventsServices: [service])
		hermes.send(LogEvent(title: "Test", properties: ["key": "value"]))

		XCTAssertEqual(service.events.count, 1)
		XCTAssertEqual(service.events.first!.title, "Test")
		XCTAssertEqual(service.events.first!.properties.count, 1)
		XCTAssertEqual(service.events.first!.properties.keys.first!, "key")
		XCTAssertEqual(service.events.first!.properties.values.first!, "value")
		XCTAssertFalse(service.events.first!.properties.isEmpty)
		XCTAssertEqual(service.events.first!.flag, .normal)
	}

	func testSendEvent_onlyTitlePropertiesFlag() {
		let service = StubEventsService()
		let hermes = Hermes(eventsServices: [service])
		hermes.send(LogEvent(title: "Test", properties: ["key": "value"], flag: .critical))

		XCTAssertEqual(service.events.count, 1)
		XCTAssertEqual(service.events.first!.title, "Test")
		XCTAssertEqual(service.events.first!.properties.count, 1)
		XCTAssertEqual(service.events.first!.properties.keys.first!, "key")
		XCTAssertEqual(service.events.first!.properties.values.first!, "value")
		XCTAssertFalse(service.events.first!.properties.isEmpty)
		XCTAssertEqual(service.events.first!.flag, .critical)
	}
}
