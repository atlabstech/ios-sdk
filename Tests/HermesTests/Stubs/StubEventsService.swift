import Hermes

class StubEventsService: EventsService {
	var user: String? = "USR"
	var events = [LogEvent]()
	var logLevel: LogLevel = .debug

	func send(_ event: LogEvent) {
		events.append(event)
	}
}
