import XCTest
import UIKit
@testable import Hermes

final class SwizzlingTests: XCTestCase {

	@objc private func buttonSelector(_ sender: UIButton) {}
	@objc private func switchSelector(_ sender: UIButton) {}

	static var allTests = [
		("testViewControllerLog", testViewControllerLog),
	]

	func testViewControllerLog() {
		let eventsService = StubEventsService()
		_ = Hermes(eventsServices: [eventsService])
		UIViewController.testLog()
		var vc: UIViewController? = UIViewController()
		vc?.viewDidLoad()
		vc?.viewDidAppear(true)
		vc = nil
		XCTAssertEqual(eventsService.events.count, 4)
		XCTAssertEqual(eventsService.events.first!.title, "Test")
		XCTAssert(eventsService.events[1].title.contains("UIViewController.viewDidLoad"))
		XCTAssert(eventsService.events[2].title.contains("UIViewController.viewDidAppear"))
		XCTAssert(eventsService.events[3].title.contains("UIViewController.deinit"))
	}

	func testButtonLog() {
		let eventsService = StubEventsService()
		_ = Hermes(eventsServices: [eventsService])
		let b = UIButton()
		b.setTitle("Test", for: .normal)
		b.addTarget(self, action: #selector(buttonSelector(_:)), for: .touchUpInside)
		b.sendActions(for: .touchUpInside)
		XCTAssertEqual(eventsService.events.count, 1)
		XCTAssertEqual(eventsService.events.first!.title, "UIButton.action")
		XCTAssertEqual(eventsService.events.first!.properties["identifier"], "Test")
		XCTAssertEqual(eventsService.events.first!.properties["event"], "NO_ACTION_EVENT_ASSOCIATED")
	}

	func testButtonLog_insideViewController() {
		let eventsService = StubEventsService()
		_ = Hermes(eventsServices: [eventsService])
		let b = UIButton()
		let vc = UIViewController()
		vc.view.addSubview(b)
		b.setTitle("Test", for: .normal)
		b.addTarget(self, action: #selector(buttonSelector(_:)), for: .touchUpInside)
		b.sendActions(for: .touchUpInside)
		XCTAssertEqual(eventsService.events.count, 2)
		XCTAssertEqual(eventsService.events[1].title, "UIViewController.UIButton.action")
		XCTAssertEqual(eventsService.events[1].properties["identifier"], "Test")
		XCTAssertEqual(eventsService.events[1].properties["event"], "NO_ACTION_EVENT_ASSOCIATED")
	}

	func testSwitchLog() {
		let eventsService = StubEventsService()
		_ = Hermes(eventsServices: [eventsService])
		let b = UISwitch()
		b.addTarget(self, action: #selector(switchSelector(_:)), for: .valueChanged)
		b.sendActions(for: .valueChanged)
		XCTAssertEqual(eventsService.events.count, 1)
		XCTAssertEqual(eventsService.events.first!.title, "UISwitch.action")
		XCTAssertEqual(eventsService.events.first!.properties["identifier"], "UISwitch.off")
		XCTAssertEqual(eventsService.events.first!.properties["event"], "NO_ACTION_EVENT_ASSOCIATED")
		b.isOn = true
		b.sendActions(for: .valueChanged)
		XCTAssertEqual(eventsService.events.count, 2)
		XCTAssertEqual(eventsService.events[1].properties["identifier"], "UISwitch.on")
	}
}
